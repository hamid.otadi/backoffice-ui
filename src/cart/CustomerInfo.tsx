import React from "react";
import {Row, Col} from "react-bootstrap";
import {useTranslation} from 'react-i18next';
import {UserDto} from "../generated/api/userApi";

export interface CustomerInfoProps {
  customer: UserDto
}

const CustomerInfo = ({customer} : CustomerInfoProps) => {
  const {t} = useTranslation();

  return(
      <Col className="values">
        <Row>
          <Col> <>{t('cart.customerInfo.id.value')}: {customer.id}</> </Col>
          <Col> <>{t('cart.customerInfo.firstName.value')}: {customer.firstName}</> </Col>
          <Col> <>{t('cart.customerInfo.lastName.value')}: {customer.lastName}</> </Col>
          <Col> <>{t('cart.customerInfo.email.value')}: {customer.email}</> </Col>
          <Col> <>{t('cart.customerInfo.phone.value')}: {customer.phone}</> </Col>
        </Row>
      </Col>
  );
};

export  default CustomerInfo;