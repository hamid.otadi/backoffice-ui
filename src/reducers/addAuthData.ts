import {ADD_AUTH_DATA} from "../actionTypes";
import {setCurrentAuthDataResponse} from "../actions/authData";
import {Token} from "../types/token";

const initialState: Token = {
  userInfo: null,
  idToken: null,
  accessToken: null,
  authenticated: false,
  isAdminCart: false,
  isAdminCurrency: false,
  isAdminPrice: false,
  isAdminAvailability: false,
  isAdminProduct: false,
  isAdminUser: false,
  isAdminOrder: false
}

export default (state: Token = initialState, action: setCurrentAuthDataResponse) : Token =>{
  switch (action.type) {
    case ADD_AUTH_DATA:
      return {
        userInfo: action.payload.userInfo,
        idToken: action.payload.idToken,
        accessToken: action.payload.accessToken,
        authenticated: action.payload.authenticated,
        isAdminCart: action.payload.isAdminCart,
        isAdminCurrency: action.payload.isAdminCurrency,
        isAdminPrice: action.payload.isAdminPrice,
        isAdminAvailability: action.payload.isAdminAvailability,
        isAdminProduct: action.payload.isAdminProduct,
        isAdminUser: action.payload.isAdminUser,
        isAdminOrder: action.payload.isAdminOrder
      }
    default:
      return state;
  }
}
