import React from "react";
import {Row, Col} from "react-bootstrap";
import {useTranslation} from 'react-i18next';
import {CustomerInfoProps} from "../cart/CustomerInfo";

const CustomerInfo = ({customer} : CustomerInfoProps) => {
  const {t} = useTranslation();

  return (
      <Col className="values">
        <Row>
          <Col> <>{t(
              'order.customerInfo.customerInfo.id.value')}: {customer.id}</> </Col>
          <Col> <>{t(
              'order.customerInfo.customerInfo.firstName.value')}: {customer.firstName}</> </Col>
          <Col> <>{t(
              'order.customerInfo.customerInfo.lastName.value')}: {customer.lastName}</> </Col>
          <Col> <>{t(
              'order.customerInfo.customerInfo.email.value')}: {customer.email}</> </Col>
          <Col> <>{t(
              'order.customerInfo.customerInfo.phone.value')}: {customer.phone}</> </Col>
        </Row>
      </Col>
  );

};

export default CustomerInfo;